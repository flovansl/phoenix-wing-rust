use crate::models::*;

mod rpcs3;

pub use self::rpcs3::*;

pub trait Runner {
    fn games(&mut self) -> Option<Vec<GameModel>>;
    fn run(&self, game: &str);
}
