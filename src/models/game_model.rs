pub struct GameModel {
    pub id: String,
    pub title: String,
    pub icon: String,
    pub pic: String,
    pub tag: String,
    pub runner: String,
}
