#[allow(clippy::all)]
mod generated_code {
    slint::include_modules!();
}

use slint::VecModel;

pub mod managers;
pub mod models;
pub mod runners;
pub use generated_code::*;
use managers::*;

fn main() {
    let phoenix_wing = PhoenixWing::new();

    let app_bar_manager = managers::AppBarManager::new(&phoenix_wing);
    let games_manager = managers::GamesManager::new(&phoenix_wing);
    let main_manager = managers::MainManager::new(&phoenix_wing);

    phoenix_wing.global::<GamesController>().on_run({
        let games_channel = games_manager.channel.clone();
        move |game| {
            games_channel
                .send(GamesMessage::Run {
                    runner: game.runner,
                    game: game.id,
                })
                .unwrap();
        }
    });

    phoenix_wing.run();

    app_bar_manager.join().unwrap();
    games_manager.join().unwrap();
    main_manager.join().unwrap();
}
