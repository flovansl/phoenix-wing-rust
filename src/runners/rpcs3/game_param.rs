use std::path::*;

use std::{fs::File, io::Read};

#[derive(Debug)]
pub struct ParamSfo {
    pub id: String,
    pub title: String,
}

fn c_string(bytes: &[u8]) -> Option<&str> {
    let bytes_without_null = match bytes.iter().position(|&b| b == 0) {
        Some(ix) => &bytes[..ix],
        None => bytes,
    };

    std::str::from_utf8(bytes_without_null).ok()
}

fn bytes_to_big_endian(bytes: &[u8]) -> u32 {
    bytes[0] as u32 | (bytes[1] as u32) << 8 | (bytes[2] as u32) << 16 | (bytes[2] as u32) << 24
}

/// Represents a PS3 game param that can be load from PARAM.SFO file
///
/// * [SFO Documentation](https://psdevwiki.com/ps3/PARAM.SFO)
#[derive(Debug, Default)]
pub struct GameParam {
    pub id: String,
    pub title: String,
}

impl GameParam {
    /// Loads and parse game params from u8 slice.
    pub fn load_from_slice(data: &[u8]) -> Option<Self> {
        // header
        let data_table_start = bytes_to_big_endian(&data[12..16]);

        // data table
        // let data_table: Vec<u8> = data[(data_table_start as usize)..(data.len())].to_vec();

        let mut data_table: Vec<Vec<u8>> = vec![];
        let mut start_index = None;

        for i in (data_table_start as usize)..(data.len()) {
            // a null marks the end of a value
            if start_index.is_some() && data[i] == 0 {
                data_table.push(data[start_index.unwrap()..i].to_vec());
                start_index = None;
                continue;
            }

            // the first entry without non is the beginning of a new value
            if start_index.is_none() && data[i] != 0 {
                start_index = Some(i);
            }
        }

        let id = c_string(&data_table[data_table.len() - 2]);
        let title = c_string(&data_table[data_table.len() - 3]);

        Some(GameParam {
            id: id?.into(),
            title: title?.into(),
        })
    }

    /// Loads and parse a PARAM.SFO file from the given path.
    ///
    /// If the file cannot be open or cannot be parsed and Error will returned.
    pub fn load_from_param_sfo<P>(path: P) -> Result<Self, std::io::Error>
    where
        P: AsRef<Path>,
    {
        let mut param_file = File::open(path)?;
        let mut data = vec![];

        param_file.read_to_end(&mut data)?;

        if let Some(param) = Self::load_from_slice(&data) {
            return Ok(param);
        }

        Result::Err(std::io::Error::new(
            std::io::ErrorKind::Other,
            "Cannot parse file",
        ))
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn load_from_slice() {}
}
