use crate::runners::Runner;

use crate::models::*;

#[derive(Default)]
pub struct Rpcs3 {}

impl Rpcs3 {
    pub fn new() -> Option<Self> {
        Some(Self {})
    }
}

impl Runner for Rpcs3 {
    fn games(&mut self) -> Option<Vec<GameModel>> {
        let mut games = vec![];

        for i in 1..16 {
            games.push(GameModel {
                id: format!("M{}", i),
                title: format!("Final Fantasy {}", i),
                icon: format!("{}/ui/assets/images/1.jpeg", env!("CARGO_MANIFEST_DIR")),
                pic: format!("{}/ui/assets/images/1.jpeg", env!("CARGO_MANIFEST_DIR")),
                tag: "PS3 | RPCS3".into(),
                runner: "RPCS3".into(),
            });
        }
        Some(games)
    }

    fn run(&self, game: &str) {
        println!("RPCS3 run {}", game);
    }
}
