mod game_param;

pub use self::game_param::*;

#[cfg(feature = "mock")]
#[path = "rpcs3/rpcs3_mock.rs"]
mod inner;

pub use self::inner::*;

#[cfg(not(feature = "mock"))]
mod inner {
    use crate::runners::Runner;
    use std::{collections::HashMap, fs, path::*, process::Command, str::FromStr, vec};

    use crate::models::GameModel;

    use super::GameParam;

    fn app_path() -> Option<PathBuf> {
        if let Ok(app_path) = PathBuf::from_str("/Applications/RPCS3.app/Contents/MacOS/rpcs3") {
            if app_path.is_file() {
                return Some(app_path);
            }
        }
        let app_path = dirs::home_dir()
            .unwrap()
            .join("Applications/RPCS3.app/Contents/MacOS/rpcs3");

        if app_path.is_file() {
            return Some(app_path);
        }

        None
    }

    #[derive(Default)]
    pub struct Rpcs3 {
        app_path: PathBuf,
        commands: HashMap<String, String>,
    }

    impl Rpcs3 {
        pub fn new() -> Option<Self> {
            Some(Self {
                app_path: app_path()?,
                commands: HashMap::new(),
            })
        }

        pub fn app_path(&self) -> &PathBuf {
            &self.app_path
        }

        pub fn data_dir(&self) -> Option<PathBuf> {
            Some(dirs::config_dir()?.join("rpcs3/"))
        }

        pub fn hdd_dir(&self) -> Option<PathBuf> {
            Some(self.data_dir()?.join("dev_hdd0/"))
        }

        pub fn disc_dir(&self) -> Option<PathBuf> {
            Some(self.hdd_dir()?.join("disc/"))
        }

        pub fn game_path(&self, base_path: PathBuf) -> PathBuf {
            base_path.join("PS3_GAME/")
        }

        pub fn icon_path(&self, base_path: PathBuf) -> PathBuf {
            self.game_path(base_path).join("ICON0.PNG")
        }

        pub fn pic_path(&self, base_path: PathBuf) -> PathBuf {
            self.game_path(base_path).join("PIC1.PNG")
        }

        pub fn param_path(&self, base_path: PathBuf) -> PathBuf {
            self.game_path(base_path).join("PARAM.SFO")
        }
    }

    impl Runner for Rpcs3 {
        fn games(&mut self) -> Option<Vec<GameModel>> {
            let mut games = vec![];

            for entry in (fs::read_dir(self.disc_dir()?).ok()?).flatten() {
                let icon_path = self.icon_path(entry.path());
                // todo: backup pic
                let pic_path = self.pic_path(entry.path());
                let param_path = self.param_path(entry.path());

                if icon_path.is_file() && param_path.is_file() {
                    println!("{:?}", param_path);
                    if let Ok(game_param) = GameParam::load_from_param_sfo(param_path.clone()) {
                        self.commands.insert(
                            game_param.id.clone(),
                            entry
                                .path()
                                .join("PS3_GAME/USRDIR/EBOOT.BIN")
                                .to_str()
                                .unwrap()
                                .into(),
                        );

                        games.push(GameModel {
                            id: game_param.id,
                            title: game_param.title,
                            icon: icon_path.to_str()?.into(),
                            pic: pic_path.to_str()?.into(),
                            tag: "PS3 | RPCS3".into(),
                            runner: "RPCS3".into(),
                        });
                    }
                }
            }

            Some(games)
        }

        fn run(&self, game: &str) {
            if let Some(game) = self.commands.get(game) {
                Command::new(&self.app_path)
                    .args(vec!["--no-gui", game.as_str()])
                    .spawn()
                    .expect("cannot run game");
            }
        }
    }
}
