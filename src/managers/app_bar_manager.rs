use slint::{ComponentHandle, Weak};
use std::{thread, time::*};

use tokio::sync::mpsc::{UnboundedReceiver, UnboundedSender};

use crate::*;

#[derive(Debug)]
pub enum AppBarMessage {
    Quit,
}

pub struct AppBarManager {
    pub channel: UnboundedSender<AppBarMessage>,
    worker: thread::JoinHandle<()>,
}

impl AppBarManager {
    pub fn new(phoenix_wing: &PhoenixWing) -> Self {
        let (channel, receiver) = tokio::sync::mpsc::unbounded_channel();

        Self {
            channel,
            worker: thread::spawn({
                let handle = phoenix_wing.as_weak();
                move || {
                    tokio::runtime::Runtime::new()
                        .unwrap()
                        .block_on(app_bar_message_loop_loop(receiver, handle))
                        .unwrap()
                }
            }),
        }
    }

    pub fn join(self) -> std::thread::Result<()> {
        let _ = self.channel.send(AppBarMessage::Quit);
        self.worker.join()
    }
}

async fn app_bar_message_loop_loop(
    mut receiver: UnboundedReceiver<AppBarMessage>,
    handle: Weak<PhoenixWing>,
) -> tokio::io::Result<()> {
    let mut timer = Instant::now();
    let timer_duration = Duration::from_secs(1);

    update_time(&handle).await;

    loop {
        if timer.elapsed() >= timer_duration {
            timer = Instant::now();

            update_time(&handle).await;
        }

        if let Ok(message) = receiver.try_recv() {
            match message {
                AppBarMessage::Quit => return Ok(()),
            }
        }
    }
}

async fn update_time(handle: &Weak<PhoenixWing>) {
    let dt = chrono::Local::now();

    handle.upgrade_in_event_loop(move |h| {
        h.global::<AppBarAdapter>()
            .set_time(dt.format("%H:%M").to_string().into())
    });
}
