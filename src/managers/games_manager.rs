use slint::{ComponentHandle, Image, Model, SharedString, Weak};
use std::{collections::HashMap, path::Path, thread};

use tokio::sync::mpsc::{UnboundedReceiver, UnboundedSender};

use crate::*;

use crate::runners::*;

#[derive(Debug)]
pub enum GamesMessage {
    Run {
        runner: SharedString,
        game: SharedString,
    },
    Quit,
}

pub struct GamesManager {
    pub channel: UnboundedSender<GamesMessage>,
    // runners: HashMap<String, Box<dyn Runner>>,
    worker: thread::JoinHandle<()>,
}

impl GamesManager {
    pub fn new(phoenix_wing: &PhoenixWing) -> Self {
        let (channel, receiver) = tokio::sync::mpsc::unbounded_channel();

        Self {
            channel,
            worker: thread::spawn({
                let handle = phoenix_wing.as_weak();
                move || {
                    tokio::runtime::Runtime::new()
                        .unwrap()
                        .block_on(games_message_loop(receiver, handle))
                        .unwrap()
                }
            }),
        }
    }

    pub fn join(self) -> std::thread::Result<()> {
        let _ = self.channel.send(GamesMessage::Quit);
        self.worker.join()
    }
}

async fn games_message_loop(
    mut receiver: UnboundedReceiver<GamesMessage>,
    handle: Weak<PhoenixWing>,
) -> tokio::io::Result<()> {
    // initializes runners
    let mut runners: HashMap<String, Box<dyn Runner>> = HashMap::new();
    if let Some(rpcs3) = Rpcs3::new() {
        runners.insert(String::from("RPCS3"), Box::new(rpcs3));
    }

    let mut games = vec![];

    for runner in runners.values_mut() {
        if let Some(mut gms) = runner.games() {
            games.append(&mut gms);
        }
    }

    // initial read in games
    handle.upgrade_in_event_loop(move |h| {
        let games_handle = h.global::<GamesAdapter>().get_games();
        let games_vec = games_handle
            .as_any()
            .downcast_ref::<VecModel<Game>>()
            .unwrap();

        games_vec.set_vec(
            games
                .into_iter()
                .map(|g| Game {
                    id: g.id.into(),
                    title: g.title.into(),
                    icon: Image::load_from_path(Path::new(g.icon.as_str()))
                        .expect("Cannot read image"),
                    pic: Image::load_from_path(Path::new(g.pic.as_str()))
                        .expect("Cannot read image"),
                    tag: Tag {
                        title: g.tag.into(),
                    },
                    runner: g.runner.into(),
                })
                .collect::<Vec<Game>>(),
        );

        h.set_is_loading(false);
    });

    loop {
        if let Ok(message) = receiver.try_recv() {
            match message {
                GamesMessage::Run { runner, game } => {
                    if let Some(runner) = runners.get(runner.as_str()) {
                        runner.run(game.as_str());
                    }
                }
                GamesMessage::Quit => return Ok(()),
            }
        }
    }
}
