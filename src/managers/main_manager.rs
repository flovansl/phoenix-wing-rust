use slint::{ComponentHandle, Weak};
use std::thread;

use gilrs::{Button, Event, EventType, Gilrs};

use tokio::sync::mpsc::{UnboundedReceiver, UnboundedSender};

use crate::*;

#[derive(Debug)]
pub enum MainMessage {
    Quit,
}

pub struct MainManager {
    pub channel: UnboundedSender<MainMessage>,
    worker: thread::JoinHandle<()>,
}

impl MainManager {
    pub fn new(phoenix_wing: &PhoenixWing) -> Self {
        let (channel, receiver) = tokio::sync::mpsc::unbounded_channel();

        Self {
            channel,
            worker: thread::spawn({
                let handle = phoenix_wing.as_weak();
                move || {
                    tokio::runtime::Runtime::new()
                        .unwrap()
                        .block_on(main_message_loop_loop(receiver, handle))
                        .unwrap()
                }
            }),
        }
    }

    pub fn join(self) -> std::thread::Result<()> {
        let _ = self.channel.send(MainMessage::Quit);
        self.worker.join()
    }
}

async fn main_message_loop_loop(
    mut receiver: UnboundedReceiver<MainMessage>,
    handle: Weak<PhoenixWing>,
) -> tokio::io::Result<()> {
    // controllers
    let mut gilrs = Gilrs::new().unwrap();

    loop {
        while let Some(Event {
            id: _,
            event,
            time: _,
        }) = gilrs.next_event()
        {
            raise_gamepad_button_event(&handle, event).await;
        }

        if let Ok(message) = receiver.try_recv() {
            match message {
                MainMessage::Quit => return Ok(()),
            }
        }
    }
}

async fn raise_gamepad_button_event<'a>(handle: &Weak<PhoenixWing>, event: EventType) {
    if let EventType::ButtonPressed(button, _) = event {
        handle.upgrade_in_event_loop(move |h| {
            let key = match button {
                Button::South => "South",
                Button::East => "East",
                Button::North => "North",
                Button::West => "West",
                Button::C => "C",
                Button::Z => "Z",
                Button::LeftTrigger => "LeftTrigger",
                Button::LeftTrigger2 => "LeftTrigger2",
                Button::RightTrigger => "RightTrigger",
                Button::RightTrigger2 => "RightTrigger2",
                Button::Select => "Select",
                Button::Start => "Start",
                Button::Mode => "Mode",
                Button::LeftThumb => "LeftThumb",
                Button::RightThumb => "RightThumb",
                Button::DPadUp => "DPadUp",
                Button::DPadDown => "DPadDown",
                Button::DPadLeft => "DPadLeft",
                Button::DPadRight => "DPadRight",
                Button::Unknown => "Unknown",
            };

            h.invoke_gamepad_button_pressed(key.into());
        });
    }
}
